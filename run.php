<?php

require 'Test.php';

$test = new Test();

try {
    $r = $test->testConnection();
} catch (\Exception $e) {
    echo $e->getMessage();
    echo PHP_EOL;
    exit(1);
}

echo $r ? 'Success' : 'Error';
echo PHP_EOL;
