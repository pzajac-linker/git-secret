<?php

declare(strict_types=1);

class Test
{
    private $apiClient;

    public function testConnection(): bool
    {
        $login = '';
        $password = '';

        $result = $this->makeRequest($login, $password);
        return $result == 'Success';
    }

    private function makeRequest($login, $password)
    {
        if (empty($login) || empty($password)) {
            throw new \Exception('No credentials provided!');
        }
        return 'Success';
    }
}
